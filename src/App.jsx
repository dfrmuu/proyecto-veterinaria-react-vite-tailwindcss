import { useState, useEffect } from "react";
import Formulario from "./components/Formulario";
import Header  from "./components/Header";
import ListadoPacientes from "./components/ListadoPacientes";


function App() {

  //Opciones para breakpoints SM-MD
  const [opcionActiva,setOpcionActiva] = useState("formulario")

  const  [pacientes, setPacientes] = useState([]);
  const  [paciente, setPaciente] = useState({});

  useEffect(() => {
    const obtenerLS = () => {
      const pacientesLS = JSON.parse(localStorage.getItem('pacientes')) ?? []
      setPacientes(pacientesLS)
    }

    obtenerLS()
  }, [])
  

  useEffect(() => {
    localStorage.setItem('pacientes', JSON.stringify(pacientes))
  }, [pacientes])
  

  function eliminarPaciente (id){
     //Enviar paciente actualizado
    const pacientesEliminados = pacientes.filter((paciente) => paciente.id !== id)
    setPacientes(pacientesEliminados)
  }

  const handleEditar = (paciente) => {
    setPaciente(paciente)
    setOpcionActiva("formulario")
  }


  return (
    <div className="h-fit py-3 px-5 mx-auto mb-3 ">
        <Header/>
        <div className="mt-6 mb-4 rounded-xl shadow-lg bg-white py-8 px-3">

          {/* Barra de opciones */}

          <div className='md:visible xl:hidden mx-auto border-slate-300 border-2 w-full rounded-xl bg-gray-200 p-2 mb-7'>
              <ul className='flex justify-center gap-4'>
                  <li className={`bg-white text-indigo-600 text-bold border-2 cursor-pointer border-indigo-200 rounded-xl p-2 px-4
                                hover:bg-indigo-300 hover:text-indigo-700 hover:border-indigo-500
                                ${opcionActiva === "formulario" ? 'bg-indigo-300' : ''}`}
                      onClick={() => setOpcionActiva("formulario")}> 
                      Registro           
                  </li>
                  <li className={`bg-white text-indigo-600 text-bold border-2 cursor-pointer border-indigo-200 rounded-xl p-2 px-4
                                hover:bg-indigo-300 hover:text-indigo-700 hover:border-indigo-500
                                ${opcionActiva === "listado" ? 'bg-indigo-300' : ''}`}
                      onClick={() => setOpcionActiva("listado")}> 
                      Listado 
                  </li>
              </ul>
          </div>

          {/* Para breakpoints pequeños */}

          <div className="visible lg:hidden flex flex-wrap">

            {opcionActiva === "formulario" && (
                <Formulario
                  pacientes = {pacientes}
                  paciente = {paciente}
                  setPacientes={setPacientes}
                  setPaciente = {setPaciente}
                />
            )} 

            {opcionActiva === "listado" && (
                <ListadoPacientes
                  pacientes = {pacientes}
                  handleEditar = {handleEditar}
                  eliminarPaciente = {eliminarPaciente}
                />
            )}

          </div>

          {/* Para breakpoints grandes */}

          <div className="hidden lg:visible lg:flex">
                <Formulario
                  pacientes = {pacientes}
                  paciente = {paciente}
                  setPacientes={setPacientes}
                  setPaciente = {setPaciente}
                />

                <ListadoPacientes
                  pacientes = {pacientes}
                  handleEditar = {handleEditar}
                  eliminarPaciente = {eliminarPaciente}
                />
          </div>


        </div>
    </div> 
  )
}

export default App
