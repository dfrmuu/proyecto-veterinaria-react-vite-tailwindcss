import { useState, useEffect } from "react";
import Error from "./Error";
import { toast } from "react-toastify";

function Formulario({pacientes,setPacientes,paciente,setPaciente}) {

    const [nombre,  setNombre] = useState('');
    const [propietario,  setPropietario] = useState('');
    const [email,  setEmail] = useState('');
    const [fecha,  setFecha] = useState('');
    const [sintomas,  setSintomas] = useState('');

    const [error, setError] = useState(false);

    useEffect(() => {
        if (Object.keys(paciente).length > 0) {
            setNombre(paciente.nombre)
            setPropietario(paciente.propietario)
            setEmail(paciente.email)
            setFecha(paciente.fecha)
            setSintomas(paciente.sintomas)
        }
    }, [paciente])
    

    const generarKey = () => {
        const random = Date.now().toString(36)
        const key = Math.random().toString(36).substring(2)

        return key + random
    }

    const handleSubmit = (e) => {
        e.preventDefault()

        //Validación formulario
        if([nombre,propietario,email,fecha,sintomas].includes('')){
            setError(true)
            return;
        }

        const objetoPaciente = {
            nombre,
            propietario,
            email,
            fecha,
            sintomas,
        }

        //¿El objeto de paciente tiene una ID?
        if(paciente.id){
  
            objetoPaciente.id = paciente.id

            //Enviar paciente actualizado
            const pacientesActualizados = pacientes.map( (pacienteState) => pacienteState.id === paciente.id ? objetoPaciente : pacienteState )
            setPacientes(pacientesActualizados)
            setPaciente({})

            toast.success("Se editó el registro")

        }else{

            //Asignar ID
            objetoPaciente.id =  generarKey()
            setPacientes([...pacientes, objetoPaciente])

            toast.success("Se realizó el registro")
        }

        //Reinicia formulario
        setNombre('');
        setPropietario('');
        setEmail('');
        setFecha('');
        setSintomas('');
    }

    return (
      <div className="mx-3 w-full lg:w-1/2"> 
         <h2 className="font-black text-2xl md:text-2xl xl:text-3xl text-center">
            Seguimiento pacientes
         </h2>

         <p className="text-lg mt-5 text-center mb-10">
            Añade pacientes y {''}
            <span className="text-indigo-600 font-bold"> Administralos</span>
        </p>

        <form  onSubmit={handleSubmit}
            className="bg-gray-200 shadow-lg border-slate-300 border-2 rounded-xl py-10 px-5 mb-10">
            {error && <Error><p>Todos los campos son obligatorios</p></Error>}

            <div className="mb-5">
                <label className="block text-gray-700 uppercase font-bold" htmlFor="mascota"> 
                     Nombre mascota
                </label>
                <input type="text"
                       id="mascota"
                       placeholder="Nombre de la mascota"
                       className="border-2 border-indigo-300 w-full p-2 mt-2 placeholder-gray-700 rounded-md"
                       value={nombre}
                       onChange = { (e) => setNombre(e.target.value)}
                />
            </div>
            <div className="mb-5">
                <label className="block text-gray-700 uppercase font-bold" htmlFor="propietario"> 
                     Nombre propietario
                </label>
                <input type="text"
                       id="propietario"
                       placeholder="Nombre del propietario"
                       className="border-2 border-indigo-300 w-full p-2 mt-2 placeholder-gray-700 rounded-md"
                       value={propietario}
                       onChange = { (e) => setPropietario(e.target.value)}
                />
            </div>
            <div className="mb-5">
                <label className="block text-gray-700 uppercase font-bold" htmlFor="email"> 
                     Email
                </label>
                <input type="email"
                       id="email"
                       placeholder="Email de contacto propietario"
                       className="border-2 border-indigo-300 w-full p-2 mt-2 placeholder-gray-700 rounded-md"
                       value={email}
                       onChange = { (e) => setEmail(e.target.value)}
                />
            </div>
            <div className="mb-5">
                <label className="block text-gray-700 uppercase font-bold" htmlFor="alta"> 
                     Alta
                </label>
                <input type="date"
                       id="alta"
                       className="border-2 border-indigo-300 w-full p-2 mt-2 rounded-md"
                       value={fecha}
                       onChange = { (e) => setFecha(e.target.value)}
                />
            </div>
            <div className="mb-5">
                <label className="block text-gray-700 uppercase font-bold" htmlFor="sintomas"> 
                     Sintomas
                </label>
                <textarea id="sintomas"
                        placeholder="Describa los sintomas de su mascota"
                        className="border-2  border-indigo-300  w-full p-2 mt-2 placeholder-gray-700 rounded-md"
                        value={sintomas}
                        onChange = { (e) => setSintomas(e.target.value)}/>
            </div>

            <input type="submit" 
                   className="w-full text-white bg-indigo-700 font-semibold p-3 rounded-md
                             hover:bg-indigo-900 uppercase cursor-pointer transition-colors" 
                   value={paciente.id ? 'Editar paciente' : 'Agregar paciente'}/>
        </form>
      </div>
    )
  }
  
  export default Formulario;