import Swal from 'sweetalert2'

function Pacientes({ paciente,eliminarPaciente, handleEditar }) {
  const { nombre, propietario, email, fecha, sintomas, id} = paciente;

  function handleEliminar() {
    Swal.fire({
      title: 'Deseas eliminar este paciente?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.isConfirmed) {
        eliminarPaciente(id)
        setPaciente({})
        Swal.fire(
          'Borrado!',
          'Se ha eliminado el paciente.',
          'success'
        )
      }
    })
  }




  return (
    <div className=" mx-5 mt-5 bg-gray-200 shadow-lg border-slate-300 border-2  rounded-xl px-5 py-10">
      <p className="font-bold mb-3 text-gray-700 uppercase">
        Nombre: 
        <span className="font-normal normal-case"> {nombre}</span>
      </p>
      <p className="font-bold mb-3 text-gray-700 uppercase">
        Propietario:
        <span className="font-normal normal-case"> {propietario}</span>
      </p>
      <p className="font-bold mb-3 text-gray-700 uppercase">
        Email:
        <span className="font-normal normal-case"> {email}</span>
      </p>
      <p className="font-bold mb-3 text-gray-700 uppercase">
        Fecha alta:
        <span className="font-normal normal-case"> {fecha}</span>
      </p>
      <p className="font-bold mb-3 text-gray-700 uppercase">
        Sintomas:
        <span className="font-normal normal-case"> {sintomas}</span>
      </p>
      <div className="flex flex-wrap md:justify-between mt-10">
        <button
           type="button"
           className="py-2 px-10 w-full mb-3 md:w-auto md:mb-0 bg-indigo-700 hover:bg-indigo-800 font-semibold text-white rounded-lg uppercase"
           onClick={() => handleEditar(paciente)}>
              Editar
        </button>
        <button
           type="button"
           className="py-2 px-10 w-full md:w-auto bg-red-600 hover:bg-red-500 font-semibold text-white rounded-lg uppercase"
           onClick = {handleEliminar}>
              Eliminar
        </button>
      </div>
    </div>
  );
}

export default Pacientes;
