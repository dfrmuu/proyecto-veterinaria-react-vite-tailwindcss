import Pacientes from "./Pacientes";

function ListadoPacientes({ pacientes, eliminarPaciente, handleEditar}) {


  return (
    <div className="w-full  md:h-auto lg:w-1/2 lg:h-[48rem] lg:block no-scrollbar lg:overflow-y-scroll">
      {pacientes && pacientes.length ? (
        <>
          <h2 className="font-black text-2xl md:text-2xl xl:text-3xl text-center">
            Listado de pacientes
          </h2>
          <p className="text-xl mt-5 mb-10 text-center">
            Administra {" "}
            <span className="text-indigo-600 font-bold">pacientes y citas</span>
          </p>

          {pacientes.map((paciente) => (
            <Pacientes 
              key={paciente.id} 
              paciente={paciente} 
              handleEditar = {handleEditar}
              eliminarPaciente = {eliminarPaciente}
            />
          ))}
        </>
      ) : (
        <>
          <h2 className="font-black text-xl md:text-2xl xl:text-3xl text-center"> No hay pacientes</h2>
          <p className="text-xl mt-5 mb-10 text-center">
            Comienza agregando pacientes {" "}
            <span className="text-indigo-600 font-bold">
              y apareceran aquí.
            </span>
          </p>
        </>
      )}
    </div>
  );
}

export default ListadoPacientes;
