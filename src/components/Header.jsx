function Header() {
    return (
        <div className="bg-white p-3 rounded-xl shadow py-8 mt-4">
            <h1 className="font-black text-2xl md:text-3xl xl:text-5xl text-center md:w-2/3 mx-auto"> 
                Seguimiento pacientes {" "}
                <span className="text-indigo-600">
                    Veterinaria 
                </span> <br/>
                <span className="text-indigo-500  text-lg md:text-2xl xl:text-xl font-bold underline">
                    Por Daniel Roa 
                </span>
            </h1>
        </div>
    )
}

export default Header;