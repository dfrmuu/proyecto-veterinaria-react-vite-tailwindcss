
function Error({children}) {
  return (
    <div className="bg-red-600 text-center p-3 font-bold text-white mb-3 rounded-md uppercase">
          {children}
    </div>
  )
}

export default Error